=== Media from FTP ===
Contributors: Katsushi Kawamori
Donate link: http://gallerylink.nyanko.org/medialink/media-from-ftp/
Tags: admin, attachment, attachments, ftp, gallery, image preview, image upload, images, import, importer, media, media library, sync, synchronize, upload, uploader
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 2.26
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Register to media library from files that have been uploaded by FTP.

== Description ==

* Register to media library from files that have been uploaded by FTP.
* This create a thumbnail of the image file.
* This create a metadata(Images, Videos, Audios).
* Change the date/time.
* Adopt [DateTimePicker](http://xdsoft.net/jqplugins/datetimepicker/). jQuery plugin select date/time.


Why I made this?
In the media uploader, you may not be able to upload by the environment of server.
That's when the files are large.
You do not mind the size of the file if FTP.

== Installation ==

1. Upload `mediafromftp` directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

none

== Screenshots ==

1. Search directory selection
2. Search file display
3. Registration file selection
4. File registration result
5. Search exclude file
6. Change the upload directory

== Changelog ==

= 2.26 =
Change management screen to responsive tab menu design.
Change /languages.

= 2.25 =
Fixed a problem of management screen.

= 2.24 =
Change the date/time.

= 2.23 =
Attachments organize into month- and year-based folders by automatic.

= 2.22 =
Change output information for images.

= 2.21 =
Fixed the problem of get the site address.

= 2.20 =
Fixed a problem of search of files on virtualhost.

= 2.19 =
Can update to use of time stamp of the file.
Change /languages.

= 2.18 =
Fixed of problem of file search and directory search.

= 2.17 =
Fixed of problem of file name with spaces.
Change /languages.

= 2.16 =
Fixed of problem of error in debug mode.

= 2.15 =
Enrich the output information.
For when the process is stopped in the middle, added the back button.
Change /languages.

= 2.14 =
Fixed a problem of search by same filename of different directoryname.
Fixed CSS.

= 2.13 =
Add setting for directory of uploading files.
Change /languages.

= 2.12 =
Add generate metadata for video / audio.

= 2.11 =
Add select all button.

= 2.10 =
Change readme.txt.

= 2.9 =
Add screenshot.
Change readme.txt.

= 2.7 =
Supported multi-byte-directoryname and multi-byte-filename.
Change /languages.

= 2.6 =
Add search button.

= 2.5 =
Fixed display.

= 2.4 =
Adding a message to a file that can not be registered.
Delete unnecessary code.
Change /languages.

= 2.3 =
Add a setting of exclude file.
Change /languages.

= 2.2 =
Find the only file types that can be registered in the media library.

= 2.1 =
Can the selection of directories to search.
Change /languages.

= 2.0 =
Can select a file in the check box.
Change /languages.
Change readme.txt.

= 1.5 =
Supported Xampp(Microsoft Windows).
Supported the file extension of the upper case.
Change /languages.

= 1.4 =
Delete unnecessary code.

= 1.3 =
Fixed the problem of metadata that occur at the time of registration of the image file.

= 1.2 =
Fixed the problem of thumbnail creation.

= 1.1 =
Specifications to exclude files that contain spaces.

= 1.0 =

== Upgrade Notice ==

= 2.26 =
= 2.25 =
= 2.24 =
= 2.23 =
= 2.22 =
= 2.21 =
= 2.20 =
= 2.19 =
= 2.18 =
= 2.17 =
= 2.16 =
= 2.15 =
= 2.14 =
= 2.13 =
= 2.12 =
= 2.11 =
= 2.10 =
= 2.9 =
= 2.7 =
= 2.6 =
= 2.5 =
= 2.4 =
= 2.3 =
= 2.2 =
= 2.1 =
= 2.0 =
= 1.5 =
= 1.4 =
= 1.3 =
= 1.2 =
= 1.1 =
= 1.0 =

