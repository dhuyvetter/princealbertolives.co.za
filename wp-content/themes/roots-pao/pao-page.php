<?php
/*
Template Name: PAO Page
*/
?>
<?php echo get_the_post_thumbnail( get_the_ID(), array(1140, 760), array('class' => 'aligncenter full-width') ); ?>
<?php get_template_part('templates/pao-page', 'header'); ?>
<?php get_template_part('templates/content', 'page'); ?>
