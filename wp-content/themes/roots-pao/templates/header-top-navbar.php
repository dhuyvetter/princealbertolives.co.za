<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container row">
    <div class="col-lg-2">
    	<img src="/wp-content/uploads/stamp.png" class="navbar-logo" alt="Prince Albert Olives roundel" >
    </div>
    <div class="navbar-header col-lg-8">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo home_url(); ?>/"><img src="/wp-content/uploads/PAO-title.png" alt="<?php bloginfo('name'); ?>"></a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
        endif;
      ?>
    </nav>
    <div class="navbar-social col-lg-2">
    	<a href="http://www.facebook.com/pages/Prince-Albert-Olives/156348904426479" title="Prince Albert Olives Facebook" ><img src="/wp-content/uploads/facebook.png" alt="Facebook logo" ></a>
    	<a href="https://twitter.com/PAOlives" title="Prince Albert Olives Twitter" ><img src="/wp-content/uploads/twitter.png" alt="Twitter logo" ></a>
    </div>
  </div>
</header>
