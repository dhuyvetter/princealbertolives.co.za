=== CSV Importer & Geocoder / Exporter & XML Exporter ===
Contributors: viadat
Requires at least: 1.0
Stable tag: 1.2.1

== Description ==
Quickly import a CSV file (spreadsheet) into your Store Locator's database for speedy set-up & export locations to a CSV file for storage of all location data.

== Installation ==
* View: http://docs.viadat.com/CSV_Importer_Geocoder_Exporter_XML_Exporter#Installation
* Export locations using export link on Locations > Manage admin page
* Import locations using the CSV Import form on the Locations > Add admin page

== Changelog ==
= 1.2.1 = 
* Added 'regeo_refresh' value to control refresh after re-geocoding
* Div -> table (class widefat) for 'Successful Import' message

= 1.2 =
* Added re-geocoding capabilities

= 1.1 =
Update for Store Locator v2.0 compatibility

= 1.0 =
* Initial release

== Frequently Asked Questions ==
Make sure to check http://docs.viadat.com/CSV_Importer_Geocoder_Exporter_XML_Exporter#Frequently_Asked_Questions for the most updated information